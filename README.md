# Indicaradores da saúde

## running locally on docker

1. build the image

```bash
docker build -t indicarores-da-saude:latest .
```

2. running the container

```bash
docker run -p 5000:5000 --name indicadores-da-saude indicarores-da-saude
```

3. access the [endpoint](http://localhost:5000/api/v1/resources/sih?uf=PR&mun=411730&mes=1&ano=2020) to get the data.