FROM ubuntu:16.04

LABEL maintainer="brunohafonso@gmail.com"

WORKDIR /app

RUN apt-get update && \
    apt-get install --assume-yes python3 python3-pip && \
    pip3 install setuptools pip --upgrade --force-reinstall

COPY . /app 

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]

CMD ["/app/wsgi.py"]