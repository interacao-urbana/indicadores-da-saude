import os
from ftplib import FTP
import Saude.utilities.libDiretorio as lbDir
import Saude.utilities.libArquivos as lbArq
import dbf as TbDBF

def downloadPA(state: str, year: int, month:str, pDiretorioInicial: str, cidade: str):
    state = state.upper()
    year2 = str(year)[-2:]
    month = str(month).zfill(2)
    ftype = 'DBC'
    downloadpath = '/dissemin/publicos/SIASUS/200801_/Dados'
    vdiretoriocriado = lbDir.Criar_Diretorio(state,pDiretorioInicial)

    ftp = FTP(host='ftp.datasus.gov.br')
    ftp.login()
    ftp.cwd(downloadpath.format(year))
    fname = 'PA{}{}{}.dbc'.format(state, str(year2).zfill(2), month)
    fname_salvar = 'PA{}{}{}.dbf'.format(state, str(year2).zfill(2), month)
    lbArq._fetch_file(fname, ftp, ftype, vdiretoriocriado, fname_salvar)
    results = Importar_BDPA(fname_salvar, vdiretoriocriado+os.path.sep,state,year,month,cidade)

    return results

def downloadBI(state: str, year: int, month:str, pDiretorioInicial: str, cidade: str):
    state = state.upper()
    year2 = str(year)[-2:]
    month = str(month).zfill(2)
    ftype = 'DBC'
    downloadpath = '/dissemin/publicos/SIASUS/200801_/Dados'
    vdiretoriocriado = lbDir.Criar_Diretorio(state,pDiretorioInicial)

    ftp = FTP(host='ftp.datasus.gov.br')
    ftp.login()
    ftp.cwd(downloadpath.format(year))
    fname = 'BI{}{}{}.dbc'.format(state, str(year2).zfill(2), month)
    fname_salvar = 'BI{}{}{}.dbf'.format(state, str(year2).zfill(2), month)
    lbArq._fetch_file(fname, ftp, ftype, vdiretoriocriado, fname_salvar)
    results = Importar_BDBI(fname_salvar, vdiretoriocriado+os.path.sep,state,year,month,cidade)

    return results

def Importar_BDPA(fname : str, pDiretorio : str,state: str, year: int, month: str, cidade : str):
     results = []
    #  if msih.Apagar_Registro_Mes_Ano(month,year,state) ==True:
     Table = TbDBF.Table(pDiretorio+fname,codepage='cp860')
     Table.open()         
     records = TbDBF.pql(Table,"select * where pa_gestao =="+"'"+cidade+"'")
     if len(records)>0:
         icont = 1
        #  PB.printProgressBar(0, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
         for dados in records:
            #  PB.printProgressBar(icont, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
             result = {
                 'pa_coduni':dados.pa_coduni,
                 'pa_gestao':dados.pa_gestao,
                 'pa_condic':dados.pa_condic,
                 'pa_ufmun':dados.pa_ufmun,
                 'pa_regct':dados.pa_regct,
                 'pa_incout':str(dados.pa_incout),
                 'pa_incurg':dados.pa_incurg,
                 'pa_tpups':dados.pa_tpups,
                 'pa_tippre':dados.pa_tippre,
                 'pa_mn_ind':dados.pa_mn_ind,
                 'pa_cnpjcpf':dados.pa_cnpjcpf,
                 'pa_cnpjmnt':dados.pa_cnpjmnt,
                 'pa_cnpj_cc':dados.pa_cnpj_cc,
                 'pa_mvm':str(dados.pa_mvm),
                 'pa_cmp':str(dados.pa_cmp),
                 'pa_proc_id':dados.pa_proc_id,
                 'pa_tpfin':dados.pa_tpfin,
                 'pa_subfin':dados.pa_subfin,
                 'pa_nivcpl':dados.pa_nivcpl,
                 'pa_docorig':dados.pa_docorig,
                 'pa_autoriz':dados.pa_autoriz,
                 'pa_cnsmed':dados.pa_cnsmed,
                 'pa_cbocod':dados.pa_cbocod,
                 'pa_motsai':dados.pa_motsai,
                 'pa_obito':dados.pa_obito,
                 'pa_encerr':dados.pa_encerr,
                 'pa_perman':dados.pa_perman,
                 'pa_alta':dados.pa_alta,
                 'pa_transf':dados.pa_transf,
                 'pa_cidpri':dados.pa_cidpri,
                 'pa_cidsec':dados.pa_cidsec,
                 'pa_cidcas':dados.pa_cidcas,
                 'pa_catend':dados.pa_catend,
                 'pa_idade':dados.pa_idade,
                 'idademin':str(dados.idademin),
                 'idademax':str(dados.idademax),
                 'pa_flidade':dados.pa_flidade,
                 'pa_sexo':dados.pa_sexo,
                 'pa_racacor':dados.pa_racacor,
                 'pa_munpcn':dados.pa_munpcn,
                 'pa_qtdpro':str(dados.pa_qtdpro),
                 'pa_qtdapr':str(dados.pa_qtdapr),
                 'pa_valpro':str(dados.pa_valpro),
                 'pa_valapr':str(dados.pa_valapr),
                 'pa_ufdif':dados.pa_ufdif,
                 'pa_mndif':dados.pa_mndif,
                 'pa_dif_val':str(dados.pa_dif_val),
                 'nu_vpa_tot':str(dados.nu_vpa_tot),
                 'nu_pa_tot':str(dados.nu_pa_tot),
                 'pa_indica':str(dados.pa_indica),
                 'pa_codoco':dados.pa_codoco,
                 'pa_flqt':dados.pa_flqt,
                 'pa_fler':dados.pa_fler,
                 'pa_etnia':dados.pa_etnia,
                 'pa_vl_cf':str(dados.pa_vl_cf),
                 'pa_vl_cl':str(dados.pa_vl_cl),
                 'pa_vl_inc':str(dados.pa_vl_inc),
                 'pa_srv_c':str(dados.pa_srv_c),
                 'pa_ine':dados.pa_ine,
                 'pa_nat_jur':dados.pa_nat_jur,
                 'estado':state.upper()
             }
             results.append(result)
             icont = icont + 1 
     Table.close() 
     print('\nImportação Finalizada SIAPA.\n') 
     return results

def Importar_BDBI(fname : str, pDiretorio : str,state: str, year: int, month: str, cidade : str):
     results = []
    #  if msih.Apagar_Registro_Mes_Ano(month,year,state) ==True:
     Table = TbDBF.Table(pDiretorio+fname,codepage='cp860')
     Table.open()         
     records = TbDBF.pql(Table,"select * where gestao =="+"'"+cidade+"'")
     if len(records)>0:
         icont = 1
        #  PB.printProgressBar(0, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
         for dados in records:
            #  PB.printProgressBar(icont, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
             result = {
                 'coduni':dados.coduni,
                 'gestao':dados.gestao,
                 'condic':dados.condic,
                 'ufmun':dados.ufmun,
                 'tpups':dados.tpups,
                 'tippre':dados.tippre,
                 'mn_ind':dados.mn_ind,
                 'cnpjcpf':dados.cnpjcpf,
                 'cnpjmnt':dados.cnpjmnt,
                 'cnpj_cc':dados.cnpj_cc,
                 'dt_process':str(dados.dt_process),
                 'dt_atend':dados.dt_atend,
                 'proc_id':dados.proc_id,
                 'tpfin':dados.tpfin,
                 'subfin':dados.subfin,
                 'complex':dados.complex,
                 'autoriz':dados.autoriz,
                 'cnsprof':dados.cnsprof,
                 'cboprof':dados.cboprof,
                 'cidpri':dados.cidpri,
                 'catend':dados.catend,
                 'cns_pac':dados.cns_pac,
                 'dtnasc':str(dados.dtnasc),
                 'tpidadepac':dados.tpidadepac,
                 'idadepac':str(dados.idadepac),
                 'sexopac':dados.sexopac,
                 'racacor':dados.racacor,
                 'munpac':dados.munpac,
                 'qt_apres':str(dados.qt_apres),
                 'qt_aprov':str(dados.qt_aprov),
                 'vl_apres':str(dados.vl_apres),
                 'vl_aprov':str(dados.vl_aprov),
                 'ufdif':dados.ufdif,
                 'mndif':dados.mndif,
                 'etnia':dados.etnia,
                 'nat_jur':dados.nat_jur,
                 'estado':state.upper()
             }
             results.append(result)
             icont = icont + 1 
     Table.close() 
     print('\nImportação Finalizada SIABI.\n') 
     return results