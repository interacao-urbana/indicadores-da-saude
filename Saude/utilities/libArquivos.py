import os
import Saude.utilities.ConverterDBCDBF as cb

def _fetch_file(fname, ftp, ftype, fDiretorio, fname_salvar):
    print("Downloading {}...".format(fname))
    vdiretorio_salvar = fDiretorio+os.path.sep
    try:
        ftp.retrbinary('RETR {}'.format(fname), open(
            vdiretorio_salvar+fname, 'wb').write)
        print('\nDownload concluído.')
    except:
        try:
            ftp.retrbinary('RETR {}'.format(fname.lower()),
                           open(vdiretorio_salvar+fname, 'wb').write)
            print('\nDownload concluído.')
        except:
            raise Exception("File {} not available".format(fname))
    if ftype == 'DBC':
        cb.Converter(vdiretorio_salvar+fname,
                     vdiretorio_salvar+fname_salvar)
        print('\nArquivo convertido.')

    os.unlink(vdiretorio_salvar+fname)