import os
from ftplib import FTP
import Saude.utilities.libDiretorio as lbDir
import Saude.utilities.libArquivos as lbArq
import dbf as TbDBF

def download(state: str, year: int, month:str, pDiretorioInicial: str, cidade: str):
    state = state.upper()
    year2 = str(year)[-2:]
    month = str(month).zfill(2)
    ftype = 'DBC'
    downloadpath = '/dissemin/publicos/SIHSUS/200801_/Dados'
    vdiretoriocriado = lbDir.Criar_Diretorio(state,pDiretorioInicial)

    ftp = FTP(host='ftp.datasus.gov.br')
    ftp.login()
    ftp.cwd(downloadpath.format(year))
    fname = 'RD{}{}{}.dbc'.format(state, str(year2).zfill(2), month)
    fname_salvar = 'RD{}{}{}.dbf'.format(state, str(year2).zfill(2), month)
    lbArq._fetch_file(fname, ftp, ftype, vdiretoriocriado, fname_salvar)
    results = Importar_BD(fname_salvar, vdiretoriocriado+os.path.sep,state,year,month,cidade)

    return results

def Importar_BD(fname : str, pDiretorio : str,state: str, year: int, month: str, cidade : str):
     results = []
    #  if msih.Apagar_Registro_Mes_Ano(month,year,state) ==True:
     Table = TbDBF.Table(pDiretorio+fname,codepage='cp860')
     Table.open()         
     records = TbDBF.pql(Table,"select * where munic_res =="+"'"+cidade+"'")
     if len(records)>0:
         icont = 1
        #  PB.printProgressBar(0, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
         for dados in records:
            #  PB.printProgressBar(icont, len(records), prefix = 'Progress:', suffix = 'Complete', length = 50)
             result = {
                 'uf_zi':dados.uf_zi,
                 'ano_cmpt':dados.ano_cmpt,
                 'mes_cmpt':dados.mes_cmpt,
                 'espec':dados.espec,
                 'cgc_hosp':dados.cgc_hosp,
                 'n_aih':dados.n_aih,
                 'ident':dados.ident,
                 'cep':dados.cep,
                 'munic_res':dados.munic_res,
                 'nasc':dados.nasc,
                 'sexo':dados.sexo,
                 'uti_mes_in':str(dados.uti_mes_in),
                 'uti_mes_an':str(dados.uti_mes_an),
                 'uti_mes_al':str(dados.uti_mes_al),
                 'uti_mes_to':str(dados.uti_mes_to),
                 'marca_uti':dados.marca_uti,
                 'uti_int_in':str(dados.uti_int_in),
                 'uti_int_an':str(dados.uti_int_an),
                 'uti_int_al':str(dados.uti_int_al),
                 'uti_int_to':str(dados.uti_int_to),
                 'diar_acom':str(dados.diar_acom),
                 'qt_diarias':str(dados.qt_diarias),
                 'proc_solic':dados.proc_solic,
                 'proc_rea':dados.proc_rea,
                 'val_sh':str(dados.val_sh),
                 'val_sp':str(dados.val_sp),
                 'val_sadt':str(dados.val_sadt),
                 'val_rn':str(dados.val_rn),
                 'val_acomp':str(dados.val_acomp),
                 'val_ortp':str(dados.val_ortp),
                 'val_sangue':str(dados.val_sangue),
                 'val_sadtsr':str(dados.val_sadtsr),
                 'val_transp':str(dados.val_transp),
                 'val_obsang':str(dados.val_obsang),
                 'val_ped1ac':str(dados.val_ped1ac),
                 'val_tot':str(dados.val_tot),
                 'val_uti':str(dados.val_uti),
                 'us_tot':str(dados.us_tot),
                 'dt_inter':str(dados.dt_inter),
                 'dt_saida':str(dados.dt_saida),
                 'diag_princ':dados.diag_princ,
                 'diag_secun':dados.diag_secun,
                 'cobranca':dados.cobranca,
                 'natureza':dados.natureza,
                 'nat_jur':dados.nat_jur,
                 'gestao':dados.gestao,
                 'rubrica':str(dados.rubrica),
                 'ind_vdrl':str(dados.ind_vdrl),
                 'munic_mov':str(dados.munic_mov),
                 'cod_idade':str(dados.cod_idade),
                 'idade':str(dados.idade),
                 'dias_perm':str(dados.dias_perm),
                 'morte':str(dados.morte),
                 'nacional':str(dados.nacional),
                 'num_proc':dados.num_proc,
                 'car_int':str(dados.car_int),
                 'tot_pt_sp':str(dados.tot_pt_sp),
                 'cpf_aut':dados.cpf_aut,
                 'homonimo':dados.homonimo,
                 'num_filhos':str(dados.num_filhos),
                 'instru':dados.instru,
                 'cid_notif':dados.cid_notif,
                 'contracep1':dados.contracep1,
                 'contracep2':dados.contracep2,
                 'gestrisco':dados.gestrisco,
                 'insc_pn':dados.insc_pn,
                 'seq_aih5':dados.seq_aih5,
                 'cbor':dados.cbor,
                 'cnaer':dados.cnaer,
                 'vincprev':dados.vincprev,
                 'gestor_cod':dados.gestor_cod,
                 'gestor_tp':dados.gestor_tp,
                 'gestor_cpf':dados.gestor_cpf,
                 'gestor_dt':dados.gestor_dt,
                 'cnes':dados.cnes,
                 'cnpj_mant':dados.cnpj_mant,
                 'infehosp':dados.infehosp,
                 'cid_asso':dados.cid_asso,
                 'cid_morte':dados.cid_morte,
                 'complex':dados.complex,
                 'financ':dados.financ,
                 'faec_tp':dados.faec_tp,
                 'regct':dados.regct,
                 'raca_cor':dados.raca_cor,
                 'etnia':dados.etnia,
                 'sequencia':str(dados.sequencia),
                 'remessa':str(dados.remessa),
                 'aud_just':str(dados.aud_just),
                 'sis_just':str(dados.sis_just),
                 'val_sh_fed':str(dados.val_sh_fed),
                 'val_sp_fed':str(dados.val_sp_fed),
                 'val_sh_ges':str(dados.val_sh_ges),
                 'val_sp_ges':str(dados.val_sp_ges),
                 'val_uci':str(dados.val_uci),
                 'marca_uci':dados.marca_uci,
                 'diagsec1':dados.diagsec1,
                 'diagsec2':dados.diagsec2,
                 'diagsec3':dados.diagsec3,
                 'diagsec4':dados.diagsec4,
                 'diagsec5':dados.diagsec5,
                 'diagsec6':dados.diagsec6,
                 'diagsec7':dados.diagsec7,
                 'diagsec8':dados.diagsec8,
                 'diagsec9':dados.diagsec9,
                 'tpdisec1':dados.tpdisec1,
                 'tpdisec2':dados.tpdisec2,
                 'tpdisec3':dados.tpdisec3,
                 'tpdisec4':dados.tpdisec4,
                 'tpdisec5':dados.tpdisec5,
                 'tpdisec6':dados.tpdisec6,
                 'tpdisec7':dados.tpdisec7,
                 'tpdisec8':dados.tpdisec8,
                 'tpdisec9':dados.tpdisec9
             }
             results.append(result)
             icont = icont + 1 
     Table.close() 
     print('\nImportação Finalizada SIH.\n') 
     return results
