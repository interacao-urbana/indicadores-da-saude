import os
import Saude.SIH as sih
import Saude.SIASUS as sia

vdiretorio = os.path.dirname(os.path.abspath(__file__))

def ImportarSiH(estado : str, ano : int, mes : str, cidade : str):
    print('\nAguarde Download......')
    return sih.download(estado,ano,mes,vdiretorio,cidade)

def ImportarSiaPA(estado : str, ano : int, mes : str, cidade : str):
    return sia.downloadPA(estado,ano,mes,vdiretorio,cidade)

def ImportarSiaBI(estado : str, ano : int, mes : str, cidade : str):
    return sia.downloadBI(estado,ano,mes,vdiretorio,cidade)