import Saude.BuscaDados as Busca
import flask
from flask import request, jsonify
import os

app = flask.Flask(__name__)
port = int(os.environ.get("PORT", 5000))
app.config["DEBUG"] = True

@app.route('/api/v1/resources/sih', methods=['GET'])
def api_filter_sih():
    query_parameters = request.args

    uf = query_parameters.get('uf')
    mun = query_parameters.get('mun')
    mes = query_parameters.get('mes')
    ano = query_parameters.get('ano')

    return jsonify(Busca.ImportarSiH(uf, ano, mes, mun))

@app.route('/api/v1/resources/siapa', methods=['GET'])
def api_filter_siapa():
    query_parameters = request.args

    uf = query_parameters.get('uf')
    mun = query_parameters.get('mun')
    mes = query_parameters.get('mes')
    ano = query_parameters.get('ano')

    return jsonify(Busca.ImportarSiaPA(uf, ano, mes, mun))

@app.route('/api/v1/resources/siabi', methods=['GET'])
def api_filter_siabi():
    query_parameters = request.args

    uf = query_parameters.get('uf')
    mun = query_parameters.get('mun')
    mes = query_parameters.get('mes')
    ano = query_parameters.get('ano')

    return jsonify(Busca.ImportarSiaBI(uf, ano, mes, mun))

if __name__ == '__main__':
    app.run()